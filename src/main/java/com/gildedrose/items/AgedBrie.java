package com.gildedrose.items;

import com.gildedrose.Item;

public class AgedBrie extends RegularItem implements GildedRoseItem {

    AgedBrie(Item item) {
        super(item);
    }

    public int updateQuality(int currentQuality) {
        currentQuality = increaseQuality(currentQuality,1);
        if (willSellDatePass()) {
            currentQuality = increaseQuality(currentQuality,1);
        }
        return currentQuality;
    }

}
