package com.gildedrose.items;

import com.gildedrose.Item;

import java.util.function.Function;

public class RegularItem implements GildedRoseItem {

    final protected int quality;
    final protected int sellIn;
    final protected String name;

    protected RegularItem(Item item) {
        this.quality = item.quality;
        this.sellIn = item.sellIn;
        this.name = item.name;
    }

    public int updateQuality(int currentQuality) {
        currentQuality = decreaseQuality(currentQuality);
        if (willSellDatePass()) {
            currentQuality = decreaseQuality(currentQuality);
        }
        return currentQuality;
    }

    @Override
    public int sellIn() {
        return this.sellIn;
    }

    @Override
    public int quality() {
        return this.quality;
    }

    @Override
    public String name() {
        return this.name;
    }

    private Integer decreaseQuality(int quality) {
        return qualityBoundaries().apply(quality - 1);
    }

    protected boolean willSellDatePass() {
        return sellIn <= 0;
    }

    public int tickSellIn() {
        return this.sellIn -1;
    }

    protected int increaseQuality(int quality, int increase) {
        return qualityBoundaries().apply( quality + increase);
    }

    private Function<Integer, Integer> qualityBoundaries() {
        return quality -> Math.max(0, Math.min(50, quality));
    }
}
