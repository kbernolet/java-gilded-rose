package com.gildedrose.items;

public class Conjured implements GildedRoseItem {

    private GildedRoseItem item;

    protected Conjured(GildedRoseItem item) {
        this.item = item;
    }

    @Override
    public int updateQuality(int quality) {
        return item.updateQuality(item.updateQuality(quality));
    }

    @Override
    public int tickSellIn() {
        return item.tickSellIn();
    }

    @Override
    public int sellIn() {
        return item.sellIn();
    }

    @Override
    public int quality() {
        return item.quality();
    }

    @Override
    public String name() {
        return "Conjured "+item.name();
    }
}
