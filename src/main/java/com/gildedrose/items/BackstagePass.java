package com.gildedrose.items;

import com.gildedrose.Item;

public class BackstagePass extends RegularItem implements GildedRoseItem {
    protected BackstagePass(Item item) {
        super(item);
    }

    public int updateQuality(int currentQuality) {
        if (willSellDatePass()) {
            return 0;
        } else if (isSellDateIsInOrLessThan(5)) {
            return increaseQuality(currentQuality, 3);
        } else if (isSellDateIsInOrLessThan(10)) {
            return increaseQuality(currentQuality,2);
        } else {
            return increaseQuality(currentQuality,1);
        }
    }

    private boolean isSellDateIsInOrLessThan(int days) {
        return sellIn <= days;
    }
}
