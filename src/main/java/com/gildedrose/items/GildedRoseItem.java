package com.gildedrose.items;

import com.gildedrose.Item;

public interface GildedRoseItem {

    default GildedRoseItem updateQualityAndSellIn() {
        int newQuality =  updateQuality(quality());
        int newSellIn = tickSellIn();
        return GildedRoseItemFactory.forItem(new Item(name(), newSellIn, newQuality));
    }

    int updateQuality(int quality);
    int tickSellIn();

    int quality();
    String name();
    int sellIn();
}
