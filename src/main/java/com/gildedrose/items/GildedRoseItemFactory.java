package com.gildedrose.items;

import com.gildedrose.Item;

public class GildedRoseItemFactory {

    public static GildedRoseItem forItem(Item item) {
        if ("Aged Brie".equals(item.name)) {
            return new AgedBrie(item);
        } else if ("Sulfuras, Hand of Ragnaros".equals(item.name)) {
            return new Sulfuras(item);
        } else if ("Backstage passes to a TAFKAL80ETC concert".equals(item.name)) {
            return new BackstagePass(item);
        } else if (item.name.startsWith("Conjured")) {
            return new Conjured(GildedRoseItemFactory.forItem(new Item(item.name.substring("Conjured ".length()), item.sellIn, item.quality)));
        } else {
            return new RegularItem(item);
        }
    }
}
