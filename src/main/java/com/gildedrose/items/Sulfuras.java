package com.gildedrose.items;

import com.gildedrose.Item;

public class Sulfuras extends RegularItem implements GildedRoseItem {

    Sulfuras(Item item) {
        super(item);
    }

    @Override
    public RegularItem updateQualityAndSellIn() {
        return this;
    }
}
