package com.gildedrose;

import com.gildedrose.items.GildedRoseItem;
import com.gildedrose.items.GildedRoseItemFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        Item[] newItems = Stream.of(items)
                .map(GildedRoseItemFactory::forItem)
                .map(GildedRoseItem::updateQualityAndSellIn)
                .map(item -> new Item(item.name(), item.sellIn(), item.quality()))
                .toArray(Item[]::new);
        System.arraycopy(newItems, 0, items, 0, items.length);
    }


    public static void main(String[] args) throws IOException {
        try {
            checkArguments(args);
            Item[] items = parseInputFile(args[0]);
            Integer days = Integer.valueOf(args[1]);
            GildedRose gildedRose = new GildedRose(items);
            for (int i = days; i > 0; i = i - 1) {
                gildedRose.updateQuality();
            }
            printResult(gildedRose);

        } catch (IncorrectArgumentsException e) {
            informUserAboutUsage();
        } catch (ParseException e) {
            informAboutFileFormat();

        }
    }

    private static void printResult(GildedRose gildedRose) {
        Stream.of(gildedRose.items)
                .map(item -> String.format("%s;%s;%s", item.name, item.sellIn, item.quality))
                .forEach(System.out::println);
    }

    private static Item[] parseInputFile(String fileName) throws ParseException {
        try {
            return Files.readAllLines(Paths.get(fileName)).stream()
                    .map(line -> line.split(";"))
                    .map(item -> new Item(item[0], Integer.valueOf(item[1].trim()), Integer.valueOf(item[2].trim())))
                    .toArray(Item[]::new);
        } catch (Exception e) {
            throw new ParseException();
        }
    }

    private static class ParseException extends Exception {
    }

    private static class IncorrectArgumentsException extends Exception {
    }

    private static void informAboutFileFormat() {
        System.out.println("The input file should contain the items name, sellIn and quality separated by semi-colons");
        System.out.println("Example:");
        System.out.println("Aged Brie; 2; 0");
    }

    private static void informUserAboutUsage() {
        System.out.println("Proper usage is to run this application with 2 arguments:");
        System.out.println("\t* an input file containing items");
        System.out.println("\t* the number of days to pass");
    }

    private static void checkArguments(String[] args) throws IncorrectArgumentsException {
        if (args.length != 2) throw new IncorrectArgumentsException();
        if (! Paths.get(args[0]).toFile().exists()) throw new IncorrectArgumentsException();
        try {
            Integer.valueOf(args[1]);
        } catch (NumberFormatException e) {
            throw new IncorrectArgumentsException();
        }
    }

}