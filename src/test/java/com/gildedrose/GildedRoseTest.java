package com.gildedrose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    @Test
    void regular_item_should_decrease_in_quality_every_day() {
        Item[] items = new Item[] { new Item("regular", 10, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(9, items[0].quality);
    }
    @Test
    void regular_item_should_decrease_the_sell_date_every_day() {
        Item[] items = new Item[] { new Item("regular", 10, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(9, items[0].sellIn);
    }
    @Test
    void items_should_decrease_quality_twice_as_fast_when_the_sell_date_has_passed() {
        Item[] items = new Item[] { new Item("regular", 0, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(8, items[0].quality);
    }

    @Test
    void items_should_never_have_a_negative_quality_1() {
        Item[] items = new Item[] { new Item("regular", 10, 0)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(0, items[0].quality);
    }
    @Test
    void items_should_never_have_a_negative_quality_2() {
        Item[] items = new Item[] { new Item("regular", 0, 1)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(0, items[0].quality);
    }

    @Test
    void brie_should_increase_in_quality_every_day_1() {
        Item[] items = new Item[] { new Item("Aged Brie", 10, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(11, items[0].quality);
    }

    @Test
    void brie_should_increase_tiwce_in_quality_when_the_sell_date_has_passed() {
        Item[] items = new Item[] { new Item("Aged Brie", 0, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(12, items[0].quality);
    }

    @Test
    void items_should_never_have_a_higher_quality_than_50() {
        Item[] items = new Item[] { new Item("Aged Brie", 10, 50)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(50, items[0].quality);
    }

    @Test
    void sulfuras_should_never_change_quality() {
        Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 10, 80)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(80, items[0].quality);
    }

    @Test
    void sulfuras_should_never_be_sold() {
        Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 10, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(10, items[0].sellIn);
    }

    @Test
    void backstage_passes_should_increase_in_quality_more_than_10_days_before_the_venue() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 20, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(11, items[0].quality);
    }

    @Test
    void backstage_passes_should_increase_2_times_in_quality_between_10_and_5_days_before_the_venue() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 10, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(12, items[0].quality);
    }

    @Test
    void backstage_passes_should_increase_3_times_in_quality_as_from_5_days_before_the_venue() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 5, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(13, items[0].quality);
    }

    @Test
    void backstage_passes_should_have_no_quality_when_then_venue_is_done() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 0, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(0, items[0].quality);
    }

}
