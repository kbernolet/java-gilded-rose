package com.gildedrose;

import com.gildedrose.items.GildedRoseItem;
import com.gildedrose.items.GildedRoseItemFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GildedRoseItemTest {

    @Test
    public void conjured_item_should_decrease_twice_every_day() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Conjured Mana Cake", 10, 10)).updateQualityAndSellIn();
        assertEquals(8, gildedRoseItem.quality());
    }

    @Test
    void conjured_item_should_decrease_the_sell_date_every_day() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Conjured Mana Cake", 10, 10)).updateQualityAndSellIn();
        assertEquals(9, gildedRoseItem.sellIn());
    }

    @Test
    void conjured_item_should_decrease_quality_twice_as_fast_when_the_sell_date_has_passed() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Conjured Mana Cake", 0, 10)).updateQualityAndSellIn();
        assertEquals(6, gildedRoseItem.quality());
    }

    @Test
    void regular_item_should_decrease_in_quality_every_day() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("regular", 10, 10)).updateQualityAndSellIn();
        assertEquals(9, gildedRoseItem.quality());
    }

    @Test
    void regular_item_should_decrease_the_sell_date_every_day() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("regular", 10, 10)).updateQualityAndSellIn();
        assertEquals(9, gildedRoseItem.sellIn());
    }

    @Test
    void items_should_decrease_quality_twice_as_fast_when_the_sell_date_has_passed() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("regular", 0, 10)).updateQualityAndSellIn();
        assertEquals(8, gildedRoseItem.quality());
    }

    @Test
    void items_should_never_have_a_negative_quality_1() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("regular", 10, 0)).updateQualityAndSellIn();
        assertEquals(0, gildedRoseItem.quality());
    }

    @Test
    void items_should_never_have_a_negative_quality_2() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("regular", 0, 1)).updateQualityAndSellIn();
        assertEquals(0, gildedRoseItem.quality());
    }

    @Test
    void brie_should_increase_in_quality_every_day_1() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Aged Brie", 10, 10)).updateQualityAndSellIn();
        assertEquals(11, gildedRoseItem.quality());
    }
    @Test
    void conjured_brie_should_increase_twice_quality_every_day_1() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Conjured Aged Brie", 10, 10)).updateQualityAndSellIn();
        assertEquals(12, gildedRoseItem.quality());
    }

    @Test
    void brie_should_increase_tiwce_in_quality_when_the_sell_date_has_passed() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Aged Brie", 0, 10)).updateQualityAndSellIn();
        assertEquals(12, gildedRoseItem.quality());
    }
    @Test
    void conjured_brie_should_increase_4_times_in_quality_when_the_sell_date_has_passed() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Conjured Aged Brie", 0, 10)).updateQualityAndSellIn();
        assertEquals(14, gildedRoseItem.quality());
    }

    @Test
    void items_should_never_have_a_higher_quality_than_50() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Aged Brie", 10, 50)).updateQualityAndSellIn();
        assertEquals(50, gildedRoseItem.quality());
    }

    @Test
    void sulfuras_should_never_change_quality() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Sulfuras, Hand of Ragnaros", 10, 80)).updateQualityAndSellIn();
        assertEquals(80, gildedRoseItem.quality());
    }

    @Test
    void sulfuras_should_never_be_sold() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Sulfuras, Hand of Ragnaros", 10, 10)).updateQualityAndSellIn();
        assertEquals(10, gildedRoseItem.sellIn());
    }

    @Test
    void backstage_passes_should_increase_in_quality_more_than_10_days_before_the_venue() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Backstage passes to a TAFKAL80ETC concert", 20, 10)).updateQualityAndSellIn();
        assertEquals(11, gildedRoseItem.quality());
    }
    @Test
    void conjured_backstage_passes_should_increase_twice_in_quality_more_than_10_days_before_the_venue() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Conjured Backstage passes to a TAFKAL80ETC concert", 20, 10)).updateQualityAndSellIn();
        assertEquals(12, gildedRoseItem.quality());
    }

    @Test
    void backstage_passes_should_increase_2_times_in_quality_between_10_and_5_days_before_the_venue() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Backstage passes to a TAFKAL80ETC concert", 10, 10)).updateQualityAndSellIn();
        assertEquals(12, gildedRoseItem.quality());
    }

    @Test
    void backstage_passes_should_increase_3_times_in_quality_as_from_5_days_before_the_venue() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Backstage passes to a TAFKAL80ETC concert", 5, 10)).updateQualityAndSellIn();
        assertEquals(13, gildedRoseItem.quality());
    }
    @Test
    void conjured_backstage_passes_should_increase_6_times_in_quality_as_from_5_days_before_the_venue() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Conjured Backstage passes to a TAFKAL80ETC concert", 5, 10)).updateQualityAndSellIn();
        assertEquals(16, gildedRoseItem.quality());
    }

    @Test
    void backstage_passes_should_have_no_quality_when_then_venue_is_done() {
        GildedRoseItem gildedRoseItem = GildedRoseItemFactory.forItem(new Item("Backstage passes to a TAFKAL80ETC concert", 0, 10)).updateQualityAndSellIn();
        assertEquals(0, gildedRoseItem.quality());
    }
}
