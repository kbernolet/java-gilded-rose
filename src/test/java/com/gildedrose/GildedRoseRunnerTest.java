package com.gildedrose;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class GildedRoseRunnerTest {

    @Test
    public void should_inform_user_when_run_without_arguments() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));
        GildedRose.main(new String[]{});

        Assertions.assertTrue(baos.toString().contains("Proper usage"));
    }
    @Test
    public void should_inform_user_when_run_with_only_1_argument() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));
        GildedRose.main(new String[]{"fileName"});

        Assertions.assertTrue(baos.toString().contains("Proper usage"));
    }

    @Test
    public void should_inform_user_when_run_with_too_many_arguments() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));
        GildedRose.main(new String[]{"fileName", "3", "4"});

        Assertions.assertTrue(baos.toString().contains("Proper usage"));
    }

    @Test
    public void should_inform_user_when_input_file_is_not_correct() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));
        GildedRose.main(new String[]{"src/test/resources/wrongFormatFile", "2"});

        Assertions.assertTrue(baos.toString().contains("semi-colons"));
    }
    @Test
    public void should_inform_user_when_second_argument_is_not_a_number() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));
        GildedRose.main(new String[]{"src/test/resources/inputFile", "blub"});

        Assertions.assertTrue(baos.toString().contains("number"));
    }

    @Test
    public void should_print_the_result_after_calculation() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));
        GildedRose.main(new String[]{"src/test/resources/inputFile", "2"});

        Assertions.assertTrue(baos.toString().contains("Sulfuras"));
    }
}
